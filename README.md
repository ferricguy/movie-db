# movie-db

A personal movie collection tracker, implemented as a React + Express app.

## Setup
**_note:_** node v10.15.3 (npm v6.4.1) required

`npm install`

Installs the dependencies for this package, and, on postinstall, installs the dependencies for the
client and server packages.

`npm run develop`

Runs the app in the development mode.

Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The API server is started at [http://localhost:8000](http://localhost:8000)

#### Future Addons/Updates

- Implement Redux
- Add User registration
- Implement Oauth & JWT
- Password recovery
- Creating, updating or deleting movies from the global list
- Add tests... way more tests!!!
