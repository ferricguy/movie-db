'use strict';

exports.seed = async (knex) => {
    // Deletes ALL existing entries
    await knex('users').del();

    // Inserts seed entries
    return knex('users').insert([
        {
            id: 1,
            name: 'Jim Gordon',
            email: 'jgordon@gothampd.net',
            pass: 'I<3Batman' // this is left not secure for demo purposes ONLY. real world application would have this obfuscated 
        }
    ]);
};
