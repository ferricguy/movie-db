'use strict';

exports.seed = async (knex) => {
    // Deletes ALL existing entries
    await knex('movies').del();

    // Inserts seed entries
    return knex('movies').insert([
        {
            id: 1,
            title: 'Us',
            description:
                'Haunted by an unexplainable and unresolved trauma from her past and compounded by a string of eerie coincidences, Adelaide feels her paranoia elevate to high-alert as she grows increasingly certain that something bad is going to befall her family. After spending a tense beach day with their friends, the Tylers (Emmy winner Elisabeth Moss, Tim Heidecker, Cali Sheldon, Noelle Sheldon), Adelaide and her family return to their vacation home. When darkness falls, the Wilsons discover the silhouette of four figures holding hands as they stand in the driveway. Us pits an endearing American family against a terrifying and uncanny opponent: doppelgängers of themselves.',
            rating: 4,
            genre: 'Horror,Mystery & Suspense',
            year: 2019,
            director: 'Jordan Peele',
            posterUrl: 'https://via.placeholder.com/150?text=Us'
        },
        {
            id: 2,
            title: 'Black Panther',
            description:
                '"Black Panther" follows T\'Challa who, after the events of "Captain America: Civil War," returns home to the isolated, technologically advanced African nation of Wakanda to take his place as King. However, when an old enemy reappears on the radar, T\'Challa\'s mettle as King and Black Panther is tested when he is drawn into a conflict that puts the entire fate of Wakanda and the world at risk.',
            rating: 5,
            genre: 'Action & Adventure,Drama,Science Fiction & Fantasy',
            year: 2018,
            director: 'Ryan Coogler',
            posterUrl: 'https://via.placeholder.com/150?text=Black+Panther'
        },
        {
            id: 3,
            title: 'Lady Bird',
            description:
                'In Lady Bird, Greta Gerwig reveals herself to be a bold new cinematic voice with her directorial debut, excavating both the humor and pathos in the turbulent bond between a mother and her teenage daughter. Christine "Lady Bird" McPherson (Saoirse Ronan) fights against but is exactly like her wildly loving, deeply opinionated and strong-willed mom (Laurie Metcalf), a nurse working tirelessly to keep her family afloat after Lady Bird\'s father (Tracy Letts) loses his job. Set in Sacramento, California in 2002, amidst a rapidly shifting American economic landscape, Lady Bird is an affecting look at the relationships that shape us, the beliefs that define us, and the unmatched beauty of a place called home.',
            rating: 3,
            genre: 'Comedy,Drama',
            year: 2017,
            director: 'Greta Gerwig',
            posterUrl: 'https://via.placeholder.com/150?text=Ladybird'
        },
        {
            id: 4,
            title: 'Moonlight',
            description:
                "The tender, heartbreaking story of a young man's struggle to find himself, told across three defining chapters in his life as he experiences the ecstasy, pain, and beauty of falling in love, while grappling with his own sexuality.",
            rating: 2,
            genre: 'Drama',
            year: 2016,
            director: 'Barry Jenkins (III)',
            posterUrl: 'https://via.placeholder.com/150?text=Moonlight'
        },
        {
            id: 5,
            title: 'Mad Max: Fury Road',
            description:
                'Filmmaker George Miller gears up for another post-apocalyptic action adventure with Fury Road, the fourth outing in the Mad Max film series. Charlize Theron stars alongside Tom Hardy (Bronson), with Zoe Kravitz, Adelaide Clemens, and Rosie Huntington Whiteley heading up the supporting cast.',
            rating: 5,
            genre: 'Action & Adventure,Science Fiction & Fantasy',
            year: 2015,
            director: 'George Miller',
            posterUrl: 'https://via.placeholder.com/150?text=Mad+Max:+Fury+Road'
        },
        {
            id: 6,
            title: 'Boyhood',
            description:
                "Filmed over 12 years with the same cast, Richard Linklater's BOYHOOD is a groundbreaking story of growing up as seen through the eyes of a child named Mason (a breakthrough performance by Ellar Coltrane), who literally grows up on screen before our eyes. Starring Ethan Hawke and Patricia Arquette as Mason's parents and newcomer Lorelei Linklater as his sister Samantha, BOYHOOD charts the rocky terrain of childhood like no other film has before. Snapshots of adolescence from road trips and family dinners to birthdays and graduations and all the moments in between become transcendent, set to a soundtrack spanning the years from Coldplay's Yellow to Arcade Fire's Deep Blue. BOYHOOD is both a nostalgic time capsule of the recent past and an ode to growing up and parenting. It's impossible to watch Mason and his family without thinking about our own journey.",
            rating: 2,
            genre: 'Drama',
            year: 2014,
            director: 'Richard Linklater',
            posterUrl: 'https://via.placeholder.com/150?text=Boyhood'
        },
        {
            id: 7,
            title: 'Gravity',
            description:
                'Gravity stars Sandra Bullock and George Clooney in a heart-pounding thriller that pulls you into the infinite and unforgiving realm of deep space. Bullock plays Dr. Ryan Stone, a brilliant medical engineer on her first shuttle mission, with veteran astronaut Matt Kowalsky (Clooney). But on a seemingly routine spacewalk, disaster strikes. The shuttle is destroyed, leaving Stone and Kowalsky completely alone - tethered to nothing but each other and spiraling out into the blackness. The deafening silence tells them they have lost any link to Earth and any chance for rescue. As fear turns to panic, every gulp of air eats away at what little oxygen is left. But the only way home may be to go further out into the terrifying expanse of space.',
            rating: 3,
            genre: 'Drama,Science Fiction & Fantasy',
            year: 2013,
            director: 'Alfonso Cuarón',
            posterUrl: 'https://via.placeholder.com/150?text=Gravity'
        },
        {
            id: 8,
            title: 'Argo',
            description:
                'Based on true events, Argo chronicles the life-or-death covert operation to rescue six Americans, which unfolded behind the scenes of the Iran hostage crisis-the truth of which was unknown by the public for decades. On November 4, 1979, as the Iranian revolution reaches its boiling point, militants storm the U.S. embassy in Tehran, taking 52 Americans hostage. But, in the midst of the chaos, six Americans manage to slip away and find refuge in the home of the Canadian ambassador. Knowing it is only a matter of time before the six are found out and likely killed, a CIA "exfiltration" specialist named Tony Mendez (Ben Affleck) comes up with a risky plan to get them safely out of the country. A plan so incredible, it could only happen in the movies.',
            rating: 4,
            genre: 'Drama,Mystery & Suspense',
            year: 2012,
            director: 'Chris Terrio',
            posterUrl: 'https://via.placeholder.com/150?text=Argo'
        }
    ]);
};
