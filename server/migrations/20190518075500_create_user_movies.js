'use strict';

exports.up = (knex) =>
    knex.schema.createTable('usermovies', (t) => {
        t.increments('id')
            .unsigned()
            .primary();
        t.integer('userid').notNull();
        t.integer('movieid').notNull();
        t.unique(['userid', 'movieid']);
        t.float('rating').nullable();
    });

exports.down = (knex) => knex.schema.dropTable('usermovies');
