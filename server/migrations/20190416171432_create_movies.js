'use strict';

exports.up = (knex) =>
    knex.schema.createTable('movies', (t) => {
        t.increments('id')
            .unsigned()
            .primary();
        t.timestamp('createdAt').defaultTo(knex.fn.now());
        t.timestamp('updatedAt').nullable();
        t.timestamp('deletedAt').nullable();

        t.string('title').notNull();
        t.text('description').nullable();
    });

exports.down = (knex) => knex.schema.dropTable('movies');
