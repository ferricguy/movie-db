'use strict';

exports.up = function(knex, Promise) {
    return knex.schema.table('movies', (t) => {
        t.text('posterUrl').nullable();
        t.integer('year').nullable();
        t.text('genre').nullable();
        t.float('rating').nullable();
        t.text('director').nullable();
    });
};

exports.down = function(knex, Promise) {
    return knex.schema.table('movies', (t) => {
        t.dropColumn('posterUrl');
        t.dropColumn('year');
        t.dropColumn('genre');
        t.dropColumn('rating');
        t.dropColumn('director');
    });
};
