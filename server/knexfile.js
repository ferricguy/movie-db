'use strict';

module.exports = {
    test: {
        client: 'sqlite3',
        useNullAsDefault: true,
        connection: {
            filename: './movies.test.sqlite3'
        }
    },
    development: {
        client: 'sqlite3',
        useNullAsDefault: true,
        connection: {
            filename: './movies.sqlite3'
        }
    }
};
