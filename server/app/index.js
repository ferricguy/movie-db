'use strict';

const express = require('express');
const cors = require('cors');
const { LoginRoutes, MovieRoutes, UserMovieRoutes } = require('./routes');

const app = express();

app.use(cors());

app.get('/', async (req, res) => {
    res.send('React Demo API');
});

app.post('/login', LoginRoutes);
app.use('/movies', MovieRoutes);
app.use('/user', UserMovieRoutes);

module.exports = app;
