'use strict';

const request = require('supertest');

const app = require('../index');
const orm = require('../orm');
const knexConfig = require('../../knexfile');

let knex;

beforeAll(async () => {
    // Initialize the ORM.
    knex = orm.init(knexConfig);

    // `directory` is relative to where the script is called from.
    await knex.migrate.latest({ directory: './migrations' });
});

afterAll(() => {
    return knex.destroy();
});

describe('Routes: login', () => {
    describe('POST /login', () => {
        it('should return 200 and return a user in data', async (done) => {
            expect(response.status).toEqual(200);
            expect(response.type).toEqual('application/json');
            expect(response.body).toHaveProperty('data');
            expect(Object.keys(response.body.data)).toEqual(
                expect.arrayContaining([
                    'name',
                ])
            );

            done();
        });
    });
});

describe('Routes: movies', () => {
    beforeAll(async () => {
        // `directory` is relative to where the script is called from.
        await knex.seed.run({ directory: './seeds' });
    });

    describe('GET /movies', () => {
        it('should return 200 and return an array of movies in data', async (done) => {
            const response = await request(app).get('/movies');

            expect(response.status).toEqual(200);
            expect(response.type).toEqual('application/json');
            expect(response.body).toHaveProperty('data');
            expect(Object.keys(response.body.data[0])).toEqual(
                expect.arrayContaining([
                    'id',
                    'title',
                    'description',
                    'director',
                    'year',
                    'rating',
                    'posterUrl',
                    'genre',
                    'createdAt',
                    'updatedAt',
                    'deletedAt'
                ])
            );

            done();
        });
    });

    describe('GET /movies/:id', () => {
        it('should return 200 and return a movie object in data if found', async (done) => {
            const response = await request(app).get('/movies/1');

            expect(response.status).toEqual(200);
            expect(response.type).toEqual('application/json');
            expect(response.body).toHaveProperty('data');
            expect(Object.keys(response.body.data)).toEqual(
                expect.arrayContaining([
                    'id',
                    'title',
                    'description',
                    'director',
                    'year',
                    'rating',
                    'posterUrl',
                    'genre',
                    'createdAt',
                    'updatedAt',
                    'deletedAt'
                ])
            );

            done();
        });

        it('should return 404 and return a message in error if not found', async (done) => {
            const response = await request(app).get('/movies/99');

            expect(response.status).toEqual(404);
            expect(response.type).toEqual('application/json');
            expect(response.body).toHaveProperty('error');

            done();
        });

        it('should return 422 and return a message in error if parameter is invalid', async (done) => {
            const response = await request(app).get('/movies/string');

            expect(response.status).toEqual(422);
            expect(response.type).toEqual('application/json');
            expect(response.body).toHaveProperty('error');

            done();
        });
    });
});
