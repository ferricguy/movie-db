'use strict';

const { Router } = require('express');
const { LoginController } = require('../controllers/login');

const routes = Router();

routes.post('/login', LoginController.login);

exports.LoginRoutes = routes;
