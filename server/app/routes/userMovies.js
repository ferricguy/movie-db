'use strict';

const { Router } = require('express');
const { UserMovieController } = require('../controllers/userMovies');

const routes = Router();

routes.get('/:userId/movies', UserMovieController.getAllUserMovies);
routes.get('/:userId/movies/:movieId', UserMovieController.getUserMovie);
routes.post('/:userId/movies/:movieId', UserMovieController.addUserMovie);
routes.put('/:userId/movies/:movieId', UserMovieController.updateUserMovie);
routes.delete('/:userId/movies/:movieId', UserMovieController.deleteUserMovie);

exports.UserMovieRoutes = routes;
