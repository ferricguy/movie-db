'use strict';

module.exports = {
    ...require('./login'),
    ...require('./movie'),
    ...require('./userMovies')
};
