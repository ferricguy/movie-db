'use strict';

const { Router } = require('express');
const { MovieController } = require('../controllers/movies');

const routes = Router();

routes.get('/', MovieController.getAllMovies);
routes.get('/:id', MovieController.getMovie);

exports.MovieRoutes = routes;
