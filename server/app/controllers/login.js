'use strict';

const { User } = require('../orm');

exports.LoginController = class LoginController {
  static async login(req, res) {
    if (req.headers['content-type'] !== 'application/json') {
      return res.status(500).json({ error : 'incorrect content-type' });
    }

    let body = '';

    req.on('data', chunk => {
        body += chunk.toString();
    });
    req.on('end', async () => {
      try {
        const userObj = JSON.parse(body);

        const usr = await User.query().findOne({ email: userObj.email, pass: userObj.pass }) || null;

        if (!usr) {
            return res.status(404).json({ error: `Could not find user with email: ${userObj.email}` });
        }

        // return a user WITHOUT the password in it...
        const retUser = {
          name: usr.name,
          email: usr.email,
          id: usr.id
        }

        return res.status(200).json({ user: retUser });
      } catch (error) {
        console.log(error);
        return res.status(500).json({ error });
      }
    });
  }
};
