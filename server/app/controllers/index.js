'use strict';

module.exports = {
    ...require('./login'),
    ...require('./movies'),
    ...require('./userMovies')
};
