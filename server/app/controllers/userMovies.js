'use strict';

const { UserMovie } = require('../orm');

exports.UserMovieController = class UserMovieController {
    static async getAllUserMovies(req, res) {
        const userMovies = await UserMovie.query()
            .where('userid', req.params.userId);
        return res.status(200).json({ data: userMovies });
    }

    static async getUserMovie(req, res) {
        const userMovie = await UserMovie.query()
        .where({ userid: req.params.userId, movieid: req.params.movieId });
        return res.status(200).json({ data: userMovie[0] });
    }

    static async addUserMovie(req, res) {
        try {
            const newUM = await UserMovie.query()
                .insert({ userid: req.params.userId, movieid: req.params.movieId }) 
            return res.status(201).send({
                success: 'true',
                message: `${req.params.movieId} added`,
                newUM
            });
        } catch (error) {
            return res.status(500).send({
                success: 'false',
                message: `error: ${error}`
            });
        }
    }

    static async updateUserMovie(req, res) {
        try {
            const updateCount = await UserMovie.query()
                .patch({ rating: req.query.rating })
                .where({ userid: req.params.userId, movieid: req.params.movieId });
            return res.status(202).send({
                success: 'true',
                message: `${updateCount} row updated`
            });
        } catch (error) {
            return res.status(500).send({
                success: 'false',
                message: `error: ${error}`
            });
        }
    }

    static async deleteUserMovie(req, res) {
        try {
            const deletedCount = await UserMovie.query()
                .delete()
                .where({ userid: req.params.userId, movieid: req.params.movieId });
            return res.status(202).send({
                success: 'true',
                message: `${deletedCount} row deleted`
            });
        } catch (error) {
            return res.status(500).send({
                success: 'false',
                message: `error: ${error}`
            });
        }
    }
};
