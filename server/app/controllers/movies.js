'use strict';

const { Movie } = require('../orm');

exports.MovieController = class MovieController {
    static async getAllMovies(req, res) {
        const movies = await Movie.query();
        return res.status(200).json({ data: movies });
    }

    static async getMovie(req, res) {
        let id = parseInt(req.params.id);

        if (isNaN(id)) {
            return res
                .status(422)
                .json({ error: `Movie id parameter: ${req.params.id} must be an integer` });
        }

        const movie = await Movie.query().findById(parseInt(id));

        if (!movie) {
            return res.status(404).json({ error: `Could not find movie with id: ${id}` });
        }

        return res.status(200).json({ data: movie });
    }
};
