'use strict';

const { Model } = require('objection');

class Movie extends Model {
    static get tableName() {
        return 'movies';
    }

    static get idColumn() {
        return 'id';
    }
}

module.exports = Movie;
