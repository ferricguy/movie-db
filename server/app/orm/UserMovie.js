'use strict';

const { Model } = require('objection');

class UserMovie extends Model {
    static get tableName() {
        return 'usermovies';
    }

    static get idColumn() {
        return 'id';
    }
}

module.exports = UserMovie;
