'use strict';

const app = require('.');
const orm = require('./orm');
const knexConfig = require('../knexfile');

// Initialize the ORM.
const knex = orm.init(knexConfig);

// Start the server.
const port = 8000;

app.listen(port, () => console.log(`API server started on port ${port}!`));
