# react-app-server

### `npm start`

Starts the server listening on port 8000.

### `npm test`

Launches the test runner in the interactive watch mode.

### `npm run migrate`

Runs the latest knex migrations.

### `npm run seed`

Bootstraps the database with seed data.
