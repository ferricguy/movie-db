import axios from 'axios';

const LOGIN_ENDPOINT = `http://localhost:8000/login`;
const USER_ENDPOINT = `http://localhost:8000/user`;
const GET_MOVIES_ENDPOINT = `http://localhost:8000/movies`;

class API {
    constructor(api) {
        this.api = api;
    }

    addUserMovie = (selection) => {
        return this.api.post(`${USER_ENDPOINT}/${selection.userId}/movies/${selection.movieId}`);
    };

    getUserMovies = (userId) => {
        return this.api.get(`${USER_ENDPOINT}/${userId}/movies`);
    };

    removeUserMovie = (selection) => {
        return this.api.delete(`${USER_ENDPOINT}/${selection.userId}/movies/${selection.movieId}`);
    };

    updateUserMovie = (selection) => {
        return this.api.put(`${USER_ENDPOINT}/${selection.userId}/movies/${selection.movieId}?rating=${selection.rating}`);
    };

    getMovies = () => {
        return this.api.get(GET_MOVIES_ENDPOINT);
    };

    login = (user) => {
        return this.api.post(LOGIN_ENDPOINT, user, {
            headers: { 'Content-Type': 'application/json' }
        });
    };
}

export default new API(axios);
