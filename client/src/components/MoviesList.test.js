import React from 'react';
import ReactDOM from 'react-dom';
import MoviesList from './MoviesList';

it('Renders with no movies', () => {
    const wrap = document.createElement('div');
    ReactDOM.render(<MoviesList />, wrap);
    ReactDOM.unmountComponentAtNode(wrap);
});

it('Renders with movies array provided', () => {
    const wrap = document.createElement('div');
    ReactDOM.render(<MoviesList movies={[{ id: 1 }]} />, wrap);
    ReactDOM.unmountComponentAtNode(wrap);
});
