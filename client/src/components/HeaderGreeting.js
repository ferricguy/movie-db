import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import API from '../services';

const HeaderGreeting = ({ title, user, setUser, toggleListType, isMainList }) => {
    const [logIn, setLogin] = useState(false);
    const [hasError, setError] = useState(false);

    // the following is only defaulted for ease of use in demo
    const [email, setEmail] = useState('jgordon@gothampd.net');
    const [pass, setPass] = useState('I<3Batman');

    useEffect(() => {
        const postData = async (email, pass) => {
            try {
                const result = await API.login({ email: email, pass: pass });
    
                if (result) {
                    const userMovieList = await API.getUserMovies(result.data.user.id);
                    setUser(result.data.user, (userMovieList.data.data || []));
                }
            } catch (error) {
                if (error) {
                    setLogin(false);
                    setError(true);
                }
            }
        };

        if (logIn && email && pass) {
            setError(false);
            postData(email, pass);
        } else {
            if (!isMainList) {
                toggleListType();
            }
            setUser({}, []);
        }
    }, [logIn, email, pass]);

    const displayUser = () => {
        return user && user.id;
    }

    return (
        <header className="ui container segment padded stackable grid">
            <div className="ten wide column">
                <h2>{title} {displayUser()
                    ? <button className="ui button" onClick={() => toggleListType()}>View {!isMainList 
                        ? 'all' 
                        : 'my personal'} movies</button>
                    : ''}</h2>
            </div>
            <div className="six wide column right aligned">
                {displayUser()
                    ? <span>
                        <h4>Welcome {user.name}!</h4>
                        <div className="ui animated fade button" onClick={() => setLogin(false)}>
                            <div className="visible content"><i className="sign-out icon"></i>Logout</div>
                            <div className="hidden content">SEE YA!</div>
                        </div>
                    </span>
                    : <span>
                        <div className="ui mini form">
                            <div className="two fields">
                                <div className={hasError ? "field error" : "field"}>
                                    <input placeholder="Email" type="email" value={email} onChange={e => setEmail(e.target.value)}></input>
                                </div>
                                <div className={hasError ? "field error" : "field"}>
                                    <input placeholder="Password" type="password" value={pass} onChange={e => setPass(e.target.value)}></input>
                                </div>
                            </div>
                            <div className="ui animated fade button" onClick={() => setLogin(true)}>
                                <div className="visible content"><i className="sign-in icon"></i>Login</div>
                                <div className="hidden content">GO!</div>
                            </div>
                        </div>
                    </span>
                }
            </div>
        </header>
    );
};

HeaderGreeting.propTypes = {
    title: PropTypes.string.isRequired,
    user: PropTypes.shape({
        id: PropTypes.number,
        name: PropTypes.string,
        email: PropTypes.string
    }),
    isMainList: PropTypes.bool
};

HeaderGreeting.defaultProps = {
    title: "",
    user: {},
    isMainList: true
};

export default HeaderGreeting;
