import React from 'react';
import ReactDOM from 'react-dom';
import HeaderGreeting from './HeaderGreeting';

it('Renders with title provided', () => {
    const wrap = document.createElement('div');
    const setUser = () => ({});

    ReactDOM.render(<HeaderGreeting title="Foo" setUser={setUser} />, wrap);
    ReactDOM.unmountComponentAtNode(wrap);
});
