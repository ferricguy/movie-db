import React, { useState, useEffect }  from 'react';
import PropTypes from 'prop-types';
import Rating from './Rating';
import API from '../services';

const MovieCard = ({ movie, user, setUser, isMainList }) => {
    const { posterUrl, title, rating, id, director, year, hasInPersonal } = movie;
    const [movieId, setMovie] = useState(0);
    const [addingMovie, setAdd] = useState(true);

    const addMovie = (movieId) => {
        setAdd(true);
        setMovie(movieId);
    }

    const removeMovie = (movieId) => {
        setAdd(false);
        setMovie(movieId);
    }

    useEffect(() => {
        const postData = async (movieId) => {
            let result = null;
            if (addingMovie) {
                result = await API.addUserMovie({ userId: user.id, movieId: movieId });
            } else {
                result = await API.removeUserMovie({ userId: user.id, movieId: movieId });
            }

            const userMovieList = await API.getUserMovies(user.id);
            setUser(user, userMovieList.data.data);

            console.log((result ? 'success' : 'failed'));
        };

        if (movieId > 0) {
            postData(movieId);
        }
    }, [movieId]);

    return (
        <div className="ui card">
            <div className="image">
                <img src={posterUrl} alt={title} />
            </div>
            <div className="content">
                <h2 className="header">{title}</h2>
                <p className="meta">Year: {year}</p>
                <Rating rating={rating} movieId={id} isMainList={isMainList} userId={user.id} />
                <p className="description">Director: {director}</p>
                {user && user.id > 0
                    ? isMainList
                        ? !hasInPersonal ? <small className="greenText" onClick={() => addMovie(id)}><i className="plus circle small icon"></i>Add</small> : ''
                        : <small className="redText" onClick={() => removeMovie(id)}><i className="minus circle small icon"></i>Remove</small>
                    : ''
                }
            </div>
        </div>
    );
};

MovieCard.propTypes = {
    movie: PropTypes.shape({
        id: PropTypes.number,
        posterUrl: PropTypes.string,
        title: PropTypes.string,
        rating: PropTypes.number,
        director: PropTypes.string,
        year: PropTypes.number
    }).isRequired,
    user: PropTypes.shape({
        id: PropTypes.number,
        name: PropTypes.string,
        email: PropTypes.string
    }),
    isMainList: PropTypes.bool
};

MovieCard.defaultProps = {
    movie: {},
    user: {},
    isMainList: true
};

export default MovieCard;
