import React from 'react';
import PropTypes from 'prop-types';
import MovieCard from './MovieCard';

const MoviesList = ({ movies, user, setUser, isMainList }) => {
    return (
        <div className="ui cards">
            {movies.length === 0
                ? <p>No movies selected</p>
                : movies
                .sort((a, b) => b.rating - a.rating)
                .map((movie) => (
                    <MovieCard key={movie.id} movie={movie} user={user} setUser={setUser} isMainList={isMainList} />
                ))}
        </div>
    );
};

MoviesList.propTypes = {
    movies: PropTypes.array,
    user: PropTypes.shape({
        id: PropTypes.number,
        name: PropTypes.string,
        email: PropTypes.string
    }),
    isMainList: PropTypes.bool
};

MoviesList.defaultProps = {
    movies: [],
    user: {},
    isMainList: true
};

export default MoviesList;
