import React from 'react';
import ReactDOM from 'react-dom';
import MovieCard from './MovieCard';

it('Renders with movie provided', () => {
    const wrap = document.createElement('div');
    ReactDOM.render(<MovieCard movie={{ id: 1 }} />, wrap);
    ReactDOM.unmountComponentAtNode(wrap);
});
