import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import API from '../services';

const Rating = ({ movieId, rating, isMainList, userId }) => {
    const [ratingValue, setRating] = useState(rating);
    let stars = [];

    useEffect(() => {
        const postData = async (ratingValue) => {
            const result = await API.updateUserMovie({ userId: userId, movieId: movieId, rating: ratingValue });
            setStars();
            console.log((result ? 'success' : 'failed'));
        };

        if (!isMainList) {
            postData(ratingValue);
        }
    }, [ratingValue]);

    const setStars = () => {
        for (let i = 0; i < ratingValue; i++) {
            stars.push(<i className="star icon no-hover yellow" key={i} onClick={() => (!isMainList ? setRating(i + 1) : setStars())} />);
        }
        for (let i = ratingValue; i < 5; i++) {
            stars.push(<i className="star outline icon yellow no-hover" key={i} onClick={() => (!isMainList ? setRating(i + 1) : setStars())} />);
        }
    }

    setStars();
    return <div className="rating">{stars}</div>;
};

Rating.propTypes = {
    movieId: PropTypes.number,
    rating: PropTypes.number,
    isMainList: PropTypes.bool,
    userId: PropTypes.number
};

Rating.defaultProps = {
    movieId: 0,
    rating: 0,
    isMainList: true,
    userId: 0
};

export default Rating;
