import React from 'react';
import ReactDOM from 'react-dom';
import Rating from './Rating';

it('Renders with no rating', () => {
    const wrap = document.createElement('div');
    ReactDOM.render(<Rating />, wrap);
    ReactDOM.unmountComponentAtNode(wrap);
});

it('Renders with rating provided', () => {
    const wrap = document.createElement('div');
    ReactDOM.render(<Rating rating={4} />, wrap);
    ReactDOM.unmountComponentAtNode(wrap);
});
