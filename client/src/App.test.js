import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';

it('Renders with title provided', () => {
    const div = document.createElement('div');
    ReactDOM.render(<App title="Foo" />, div);
    ReactDOM.unmountComponentAtNode(div);
});
