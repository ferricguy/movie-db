import React from 'react';
import PropTypes from 'prop-types';
import { HeaderGreeting, MoviesList } from './components';
import API from './services';
import './App.css';

class App extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            movies: [],
            personalMovies: [],
            userMovies: [],
            isMainList: true,
            user: {}
        };

        this.toggleListType = this.toggleListType.bind(this);
        this.setUser = this.setUser.bind(this);
    }

    setUser(newUser, userMovieList){
        let movieList = [];

        if (userMovieList) {
            userMovieList.forEach((um) => {
                let movie = this.state.movies.find((mov) => (mov.id === um.movieid));
                movie.hasInPersonal = false;

                if (this.state.isMainList) {
                    movie.hasInPersonal = true;
                } else {
                    movie.rating = (um.rating === null ? 0 : um.rating);
                }
    
                movieList.push(movie);
            });

            this.setState({ personalMovies: movieList });
        }

        this.setState({
            user: newUser,
            userMovies: userMovieList
        });
    }

    toggleListType(){
        this.setState({
            isMainList: !this.state.isMainList
        });
    }

    async componentDidMount() {
        try {
            const {
                data: { data: movies }
            } = await API.getMovies();

            if (movies) {
                this.setState({ movies });
            }
        } catch (error) {
            console.log(error);
        }
    }

    render() {
        const { title } = this.props;
        const { movies, personalMovies, userMovies, user, isMainList } = this.state;

        return (
            <div id="wrap" className="ui container padded">
                <HeaderGreeting title={title} user={user} setUser={this.setUser} isMainList={isMainList} toggleListType={this.toggleListType}/>
                <main className="ui container segment very padded">
                    {isMainList
                        ? <MoviesList movies={movies} userMovies={userMovies} user={user} setUser={this.setUser} isMainList={isMainList} />
                        : <MoviesList movies={personalMovies} userMovies={userMovies} user={user} setUser={this.setUser} isMainList={isMainList} />
                    }                    
                </main>
            </div>
        );
    }
}

App.propTypes = {
    title: PropTypes.string.isRequired
};

export default App;
